# API V1

## Описание предварительных методов
Данный раздел описывает методы для проведения сценария заказа на тестовом стенде.
### Получение токена по credentials
Метод получения авторизационного токена по паре login:password. Используется basic авторизация. Пара login: password передается в Authorization header в base64 кодировке.  
**Рекомендуется** обновлять токен за пару минут до истечения срока жизни токена или при получении ошибки 401 INVALID_CREDENTIALS.  

Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/authentication/token
```  
Пример запроса:  
``` py 
curl -X POST "https://partners-api-sandbox.samokat.io/showcase/v1/authentication/token" -H "accept: */*"  
-H "authorization: Basic ZGVtbzpwQDU1dzByZA=="
``` 
Пример ответа:  
``` py 
{
    "token": "181210f8f9c779c26da1d9b2075bde0127302ee0e3fca38c9a83f5b1dd8e5d3b",
    "expiresAt": "2018-03-21T12:47:25+03:00"
}
``` 
**Запрос**

| Имя           | Значение                       | Обязательность | Где    | Описание          | Пример                                                                   |
|---------------|--------------------------------|----------------|--------|-------------------|--------------------------------------------------------------------------|
| authorization | Basic <base64(login: password)>| Да             | header | Basic авторизация | Basic ZGVtbzpwQ DU1dzByZA= = (пример получен из данных “demo:p@55 w0rd”) |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                          | Message                                   | Значение                                    |
|-------------|-------------------------------|-------------------------------------------|---------------------------------------------|
| 200         |                               |                                           | Успешный ответ                              |
| 400         | INVALID_AUTHORIZA TION_HEADER | authorization header cannot be read       | Некорректный формат авторизационного токена |
| 401         | EMPTY_AUTHORIZAT ION_HEADER   | authorization header cannot be empty      | Авторизационный токен не передан            |
| 401         | INVALID_CREDENTIALS           | credentials are invalid                   | Некорректные login и/или password           |
| 500         | INTERNAL_SERVER_ ERROR        | something wrong. please try again later   | Внутренняя ошибка сервера                   |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
``` 

### Получение информации о складах
Метод получения информации о всех складах.

Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/store
```  
Пример запроса:  
``` py 
curl --location 'https://partners-api-sandbox.samokat.io/showcase/v1/stores' \
--header 'Accept: */*' \
--header 'Authorization: Bearer 69d04bbe47b311385509dbd06ce2aa9a3e921806b5b12c7287f8ba17803f7f2f'
``` 
Пример ответа:  
``` py 
{
    "storeId": "5e5aaf5a-90aa-11ea-b6ec-0050560306e1",
    "area": {
        "areaId": "cb6aa8c5-206e-4de5-b2f6-9b65f9ed3a08",
        "updatedAt": "2023-04-05T12:34:01Z",
        "feature": {
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            37.743275,
                            55.653117
                        ],
                        [
                            37.743757,
                            55.651699
                        ] 
                        ...
            }
        }
    }
}
``` 
**Запрос**

| Имя           | Значение            | Обязательность | Где    | Описание                 | Пример                                                                                       |
|---------------|---------------------|----------------|--------|--------------------------|----------------------------------------------------------------------------------------------|
| authorization | Token type + token  | Да             | header | Токен доступа к ресурсу  | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b  |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                        | Message                                 | Значение                          |
|-------------|-----------------------------|-----------------------------------------|-----------------------------------|
| 200         |                             |                                         | Успешный ответ                    |
| 400         | UNAUTHORIZED                |                                         | Неавторизованный запрос           |
| 500         | INTERNAL_SERVER_ ERROR      | something wrong. please try again later | Внутренняя ошибка сервера         |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
``` 


### Получение информации о складах с приостановленной доставкой
Метод получения информации о складах с приостановленной доставкой.

Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/suspended
```  
Пример запроса:  
``` py 
curl --location 'https://partners-api-sandbox.samokat.io/showcase/v1/stores/suspended' \
--header 'Accept: */*' \
--header 'Authorization: Bearer 69d04bbe47b311385509dbd06ce2aa9a3e921806b5b12c7287f8ba17803f7f2f'
``` 
Пример ответа:  
``` py 
{
    "stores": [
        "5542654c-b343-11eb-85ab-1c34dae33151"
        ...
    ]
}
``` 
**Запрос**

| Имя           | Значение            | Обязательность | Где    | Описание                 | Пример                                                                                       |
|---------------|---------------------|----------------|--------|--------------------------|----------------------------------------------------------------------------------------------|
| authorization | Token type + token  | Да             | header | Токен доступа к ресурсу  | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b  |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                        | Message                                 | Значение                          |
|-------------|-----------------------------|-----------------------------------------|-----------------------------------|
| 200         |                             |                                         | Успешный ответ                    |
| 401         | UNAUTHORIZED                |                                         | Неавторизованный запрос           |
| 500         | INTERNAL_SERVER_ ERROR      | something wrong. please try again later | Внутренняя ошибка сервера         |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
``` 


### Получение SLA и минимальной суммы доставки по координатам точки
Метод получения информации об SLA (Service Level Agreement) и минимальной сумме заказа по координате точки.
Геокодирование выполняется на стороне Партнера.

Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/delivery?lat=XX.XXXX&lon=YY.YYYY
```  
Пример запроса:  
``` py 
curl --location 'https://partners-api-sandbox.samokat.io/showcase/v1/delivery?lat=55.6597&lon=37.7466' \
--header 'Accept: */*' \
--header 'Authorization: Bearer 69d04bbe47b311385509dbd06ce2aa9a3e921806b5b12c7287f8ba17803f7f2f'
``` 
Пример ответа:  
``` py 
{
    "sla": 15,
    "minimalAmount": 24000,
    "store": {
        "isOpen": true
    }
}
``` 
**Запрос**

| Имя           | Значение           | Обязательность | Где    | Описание                | Пример                                                                                      |
|---------------|--------------------|----------------|--------|-------------------------|---------------------------------------------------------------------------------------------|
| authorization | Token type + token | Да             | header | Токен доступа к ресурсу | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b |
| lat           | Double             | Да             | query  | Значение широты         | 59.99761275 899162                                                                          |
| lon           | Double             | Да             | query  | Значение долготы        | 30.38526535 0341797                                                                         |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                   | Message                                                | Message                                               |
|-------------|------------------------|--------------------------------------------------------|-------------------------------------------------------|
| 200         |                        |                                                        | Успешный ответ                                        |
| 401         | UNAUTHORIZED           |                                                        | Неавторизованный запрос                               |
| 404         | AREA_NOT_FOUND         | cannot found area by coordinates: lat=$lat long=$long  | Зона обслуживания не найдена по заданным координатам  |
| 500         | INTERNAL_SERVER_ ERROR | something wrong. please try again later                | Внутренняя ошибка сервера                             |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```

### Загрузка категорий
Метод получения категорий товаров.

Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/{storeId}/catalog/categories
```  
Пример запроса:  
``` py 
curl --location 'https://partners-api-sandbox.samokat.io/showcase/v1/stores/2c3f7283-1cf8-11ea-8d70-0050560306e1/catalog/categories?limit=10&offset=0' \
--header 'Accept: */*' \
--header 'Authorization: Bearer e45ca59e66f89319ff9b318bba417102538f5d9c09eee08a457a04772739f16e'
``` 
Пример ответа:  
``` py 
{
    "categories": [
        {
            "id": "3eb87dda-6b9e-4232-8833-f2bdad8b1a02",
            "name": "Фичер",
            "parentId": "b6b90490-9525-82a5-1bbd-1057eed67ebe",
            "images": []
        },
        {
            "id": "fbbea33e-312f-404b-a842-a4e6a0bfa926",
            "name": "Ситуативное",
            "parentId": "b6b90490-9525-82a5-1bbd-1057eed67ebe",
            "images": []
        },
        ...
    ]
}
``` 
**Запрос**

| Имя            | Значение           | Обязательность | Где    | Описание                   | Пример                                                                                      |
|----------------|--------------------|----------------|--------|----------------------------|---------------------------------------------------------------------------------------------|
| authorization  | Token type + token | Да             | header | Токен доступа к ресурсу    | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b |
| storeId        | UUID               | Да             | path   | Идентификат ор склада      | d6e24282-c4e3-11ea-b7de-0050560306e1                                                        |
| limit          | Int                | Нет            | query  | Количество данных в выдаче | 10                                                                                          |
| offset         | Int                | Нет            | query  | Смещение                   | 0                                                                                           |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                   | Message                                                | Message                                               |
|-------------|------------------------|--------------------------------------------------------|-------------------------------------------------------|
| 200         |                        |                                                        | Успешный ответ                                        |
| 401         | UNAUTHORIZED           |                                                        | Неавторизованный запрос                               |
| 500         | INTERNAL_SERVER_ ERROR | something wrong. please try again later                | Внутренняя ошибка сервера                             |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```


### Загрузка остатков товаров
Метод получения информации по остаткам товаров на складе.

Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/{storeId}/stocks
```  
Пример запроса:  
``` py 
curl --location 'https://partners-api-sandbox.samokat.io/showcase/v1/stores/d6e24282-c4e3-11ea-b7de-0050560306e1/stocks' \
--header 'Accept: */*' \
--header 'Authorization: Bearer cb25cdca63d52833cc2e984a574d459e5a2f74a39ed5620897676f1479e3e265'
``` 
Пример ответа:  
``` py 
{
    "stocks": [
        {
            "productId": "4924d68f-f1d6-11eb-85ad-1c34dae33151",
            "quantity": 10000
        },
        {
            "productId": "86fba97b-dda0-11ec-ae71-08c0eb320147",
            "quantity": 14
        },
        {
            "productId": "8e3fa354-7591-11e9-80c5-0cc47a817925",
            "quantity": 217
        },
        {
            "productId": "3003f887-9897-11ec-b0fc-08c0eb31fffb",
            "quantity": 110
        },
        ...
}
``` 
**Запрос**

| Имя                  | Значение                             | Обязательность | Где    | Описание                                                       | Пример                                                                                      |
|----------------------|--------------------------------------|----------------|--------|----------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| authorization        | Token type + token                   | Да             | header | Токен доступа к ресурсу                                        | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b |
| storeId              | UUID                                 | Да             | path   | Идентификат ор склада                                          | d6e24282-c4e3-11ea-b7de-0050560306e1                                                        |
| changedAfter         | String. Формат даты ISO with offset  | Нет            | query  | Дата, начиная с которой нужно выгрузить изменившие ся остатки  | 2011-12-03T 10:15:30+01: 00                                                                 |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                   | Message                                                | Message                                               |
|-------------|------------------------|--------------------------------------------------------|-------------------------------------------------------|
| 200         |                        |                                                        | Успешный ответ                                        |
| 401         | UNAUTHORIZED           |                                                        | Неавторизованный запрос                               |
| 500         | INTERNAL_SERVER_ ERROR | something wrong. please try again later                | Внутренняя ошибка сервера                             |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```

### Получения цен и остатков по списку товаров
Метод получения цен и остатков по списку товаров.

Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/d6e24282-c4e3-11ea-b7de-0050560306e1/products?product=114b8378-169f-11eb-8599-1c34dae33151
```  
Пример запроса:  
``` py 
curl --location 'https://partners-api-sandbox.samokat.io/showcase/v1/stores/d6e24282-c4e3-11ea-b7de-0050560306e1/stocks' \
--header 'Accept: */*' \
--header 'Authorization: Bearer cb25cdca63d52833cc2e984a574d459e5a2f74a39ed5620897676f1479e3e265'
``` 
Пример ответа:  
``` py 
{
    "products": [
        {
            "id": "114b8378-169f-11eb-8599-1c34dae33151",
            "vat": 10,
            "price": 47600,
            "quantity": 1,
            "discountPrice": 47600
        }
    ]
}
``` 
**Запрос**

| Имя               | Значение           | Обязательность | Где    | Описание                  | Пример                                                                                      |
|-------------------|--------------------|----------------|--------|---------------------------|---------------------------------------------------------------------------------------------|
| authorization     | Token type + token | Да             | header | Токен доступа к ресурсу   | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b |
| storeId           | UUID               | Да             | path   | Идентификат ор склада     | d6e24282-c4e3-11ea-b7de-0050560306e1                                                        |
| product           | Array (string)     | Да             | query  | Идентификатор  товара     | 114b8378-169f-11eb-8599-1c34dae33151                                                        |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                   | Message                                 | Message                                 |
|-------------|------------------------|-----------------------------------------|-----------------------------------------|
| 200         |                        |                                         | Успешный ответ                          |
| 400         | TOO_MANY_PROD          | Too many product IDs sent. The maximum  | Передано слишком много идентификаторов  |
| 401         | UNAUTHORIZED           |                                         | Неавторизованный запрос                 |
| 500         | INTERNAL_SERVER_ ERROR | something wrong. please try again later | Внутренняя ошибка сервера               |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```
## Оформление заказа
Данный раздел описывает методы работы с заказом: создание неподтвержденного заказа, подтверждение, отмена, получение информации о заказе.
### Создание заказа


Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/{storeId}/orders
```  
Пример запроса:  
``` py 
curl -X POST  
"https://partners-api-sandbox.samokat.io/showcase/v1/stores/04b6834f-f3c9-4034- 88bf-7c980ee295d9/orders" -H  
"accept: */*" -H "Authorization: Bearer 181210f8f9c779c26da1d9b2075bde0127302ee0e3fca38c9a83f5b1dd8e5d3b" -H  
"Content-Type: application/json" -d "{ \"comment\": \"test\", \"customer\": {\"mobile\": \"+75589966985\",   
\"name\": \"Иванов Сергей\" }, \"delivery\": { \"address\": { \"block\": 2, \"city\": \"Санкт-Петербург\",   
\"entrance\": 1, \"flat\": 4, \"floor\": \"2b\", \"house\": 34, \"intercom\": 4, \"region\": \"Ленинградская обл\",  
\"settlement\": \"Микрорайон 1\", \"street\": \"Ленина\" }, \"coordinates\": { \"lat\": 59.853226,  
\"lon\": 30.265055 } }, \"discountAmount\": 0, \"orderLines\": [ { \"discountAmount\": 0, \"discountPrice\": 2500,  
\"price\": 2500, \"productId\": \"2a2b4139-1c17-11e9-80c5-0cc47a817925\", \"quantity\": 1, \"totalDiscountAmount\": 0,   
\"totalDiscountPrice\": 2500, \"totalPrice\": 2500 } ], \"originalOrderId\": \"123asdzxc5558\",   
\"totalDiscountPrice\": 2500, \"totalOrderPrice\": 2500, \"totalPrice\": 2500}"
``` 
Пример тела запроса:  
``` py 
{
  "customer": {
    "mobile": "+76935478237",
    "name": "kshudabaevtest"
  },
  "delivery": {
    "address": {
      "city": "Санкт-Петербург",
      "comment": "kshudabaevtest530",
      "flat": "666",
      "house": "666",
      "street": "ул Тест 55"
    },
    "coordinates": {
      "lat": 55.8647,
      "lon":  37.4875
    }
  },
  
  "orderLines": [
    {
      "discountAmount": 0,
      "discountPrice": 0,
      "price": 0,
      "productId": "f69d7f59-ea62-11ed-b10a-08c0eb31fffb",
      "quantity": 1,
      "totalDiscountAmount": 0,
      "totalDiscountPrice": 0,
      "totalPrice": 0
    }
  ],
  "originalOrderId": "kshudabaevtest_{{$randomInt}}",
  "totalDiscountPrice": 0,
  "totalOrderPrice": 0,
  "totalPrice": 0,
  "discountAmount": 0,
  "comment": "kshudabaevtest530"
}
``` 
Пример ответа:  
``` py 
{
    "id": "7ccff128-88e4-47a2-9f9f-8529609776eb",
    "originalOrderId": "kshudabaevtest_887",
    "status": "created",
    "orderLinesChanged": false,
    "orderLines": [
        {
            "productId": "f69d7f59-ea62-11ed-b10a-08c0eb31fffb",
            "price": 0,
            "totalPrice": 0,
            "discountPrice": 0,
            "totalDiscountPrice": 0,
            "quantity": 1,
            "originalQuantity": 1,
            "identificationMark": null,
            "updatedDateTime": "2023-06-08T06:35:41Z"
        }
    ],
    "createdDateTime": "2023-06-08T06:35:41Z",
    "deliveredDateTime": null,
    "expectedDateTime": null,
    "updatedDateTime": "2023-06-08T06:35:41Z",
    "totalDiscountPrice": 0,
    "totalPrice": 0,
    "deliveryPrice": 0
}
``` 
**Запрос**

| Имя                            | Значение           | Обязательность | Где    | Описание                                                                                                                                | Пример                                                                                                            |
|--------------------------------|--------------------|----------------|--------|-----------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| authorization                  | Token type + token | Да             | header | Токен доступа к ресурсу                                                                                                                 | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b                       |
| storeId                        | UUID               | Да             | path   | Идентификатор склада                                                                                                                    | d6e24282-c4e3-11ea-b7de-0050560306e1                                                                              |
| comment                        | String (1000 max)  | Нет            | body   | Комментари й к заказу. Используетс я для показа сборщику и курьеру                                                                      | Просьба не звонить в дверной звонок                                                                               |
| customer.name                  | String             | Да             | body   | Имя пользователя                                                                                                                        | Иванов Иван Иванович                                                                                              |
| customer.mobile                | String             | Да             | body   | Телефон пользовател я в формате +<код страны><номер телефона> (Номера любых стран принимаются). Номер используется для связи с клиентом | +70000000000                                                                                                      |
| delivery.address               | Object             | Да             | body   | Описание доставки                                                                                                                       | 114b8378-169f-11eb-8599-1c34dae33151                                                                              |
| delivery.address.block         | String             | Нет            | body   | Номер корпуса                                                                                                                           | 2                                                                                                                 |
| delivery.address.city          | String             | Да             | body   | Город                                                                                                                                   | Санкт-Петербург                                                                                                   |
| delivery.address.comment       | String             | Нет            | body   | Комментарий к адресу                                                                                                                    | Вход со двора                                                                                                     |
| delivery.address.entrance      | String             | Нет            | body   | Вход (номер подъезда)                                                                                                                   | 1                                                                                                                 |
| delivery.address.flat          | String             | Нет            | body   | Номер квартиры                                                                                                                          | 4                                                                                                                 |
| delivery.address.floor         | String             | Нет            | body   | Этаж                                                                                                                                    | 2                                                                                                                 |
| delivery.address.house         | String             | Нет            | body   | Номер дома                                                                                                                              | “34” или “34/2” - корпус можно указать в номере дома вместо delivery.address.b lock или “34 корпус 2, строение 1” |
| delivery.address.intercom      | String             | Нет            | body   | Код домофона                                                                                                                            | 123                                                                                                               |
| delivery.address.region        | String             | Нет            | body   | Регион                                                                                                                                  | Ленинградская обл.                                                                                                |
| delivery.address.street        | String             | Нет            | body   | Улица                                                                                                                                   | Центральная                                                                                                       |
| delivery.address.settlement    | String             | Нет            | body   | Населенный пункт                                                                                                                        | Санкт-Петербург                                                                                                   |
| delivery.coordinates.lat       | Double             | Да             | body   | Широта                                                                                                                                  | 59.975533                                                                                                         |
| delivery.coordinates.lon       | Double             | Да             | body   | Долгота                                                                                                                                 | 30.28717                                                                                                          |
| discountAmount                 | Int                | Да             | body   | Сумма скидки на заказ (если скидок несколько - то общая сумма)                                                                          | 150                                                                                                               |
| orderLines                     | Array              | Да             | body   | Список позиций заказа                                                                                                                   |                                                                                                                   |
| orderLines.discountAmount      | Int                | Да             | body   | Размер скидки на единицу товара                                                                                                         | 0                                                                                                                 |
| orderLines.discountPrice       | Int                | Да             | body   | Цена товара с учетом скидки за единицу в копейках                                                                                       | 100                                                                                                               |
| orderLines.productId           | UUID               | Да             | body   | Идентификатор товара                                                                                                                    | 5500bc4b-c769-4 309-8fda-5605c96 6f2a1                                                                            |
| orderLines.quantity            | Int                | Да             | body   | Количество товара                                                                                                                       | 1                                                                                                                 |
| orderLines.totalDiscountAmount | Int                | Да             | body   | Размер скидки на позицию                                                                                                                | 0                                                                                                                 |
| orderLines.totalDiscountPrice  | Int                | Да             | body   | Цена позиции с учетом скидки в копейках                                                                                                 | 10                                                                                                                |
| orderLines.totalPrice          | Int                | Да             | body   | Цена позиции в копейках                                                                                                                 | 30                                                                                                                |
| originalOrderId                | String (255 max)   | Да             | body   | Внешний идентификатор заказа (уникальный), формируется системой Партнера                                                                | 123asdzxc                                                                                                         |
| totalDiscountPrice             | Int                | Да             | body   | Цена заказа с учетом скидок на позиции в копейках                                                                                       | 30                                                                                                                |
| totalOrderPrice                | Int                | Да             | body   | Цена заказа с учетом скидок на заказ и на позиции в копейках                                                                            | 10                                                                                                                |
| totalPrice                     | Int                | Да             | body   | Цена заказа в копейках                                                                                                                  | 900                                                                                                               |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                               | Message                                  | Message                                                                                 |
|-------------|------------------------------------|------------------------------------------|-----------------------------------------------------------------------------------------|
| 200         |                                    |                                          | Успешный ответ                                                                          |
| 400         | STORE_TEMPORARILY_NOT_AVAILABLE    | Too many product IDs sent. The maximum   | Создание заказов для этого склада временно недоступно                                   |
| 400         | NOT_ENOUGH_QUANTITY                | Not enough quantity                      | Не осталось позиций с позитивным количеством                                            |
| 400         | ORDER_ALREADY_EXISTS               | Order already exists                     | Заказ с таким id уже существует                                                         |
| 400         | ORDER_LINES_NOT_UNIQUE             | Order lines not unique                   | Строки заказа не уникальны. Строка с одной и той же позицией встречается несколько раз. |
| 400         | MIN_ORDER_PRICE_NOT_REACHED        | Minimal order price not reached          | Минимальная сумма заказа не достигнута.                                                 |
| 400         | STORE_IS_CLOSED                    | Store is closed                          | Склад закрыт                                                                            |
| 401         | UNAUTHORIZED                       | Store is closed                          | Неавторизованный запрос                                                                 |
| 500         | INTERNAL_SERVER_ERROR              | something wrong. please try again later  | Внутренняя ошибка сервера                                                               |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```
### Подтверждение заказа
Метод подтверждения ранее созданного, неподтвержденного, заказа.
Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/{storeId}/orders/{orderId}/confirmation
```  
Пример запроса:  
``` py 
curl --location --request PUT 'https://partners-api-sandbox.samokat.io/showcase/v1/stores/5e5aaf5a-90aa-11ea-b6ec-0050560306e1/orders/7ccff128-88e4-47a2-9f9f-8529609776eb/confirmation' \  
--header 'Content-Type: application/json' \  
--header 'Accept: */*' \  
--header 'Authorization: Bearer 86ea424634665840039fec370a9bd58fa6afc52a83baf51b4dec86dc97fdf838' \  
--data '{  
  "payment": {  
    "totalOrderPrice": 0,  
    "type": "prepaid"  
  }  
}  
``` 
Пример тела запроса:  
``` py 
{
  "payment": {
    "totalOrderPrice": 2500,
    "type": "prepaid"
  }
}
``` 
Пример ответа:  
``` py 
{
    "id": "0d15a7e5-988e-4672-a9ec-ce3dffaa88e4",
    "originalOrderId": "kshudabaevtest_204",
    "status": "accepted",
    "orderLinesChanged": false,
    "orderLines": [
        {
            "productId": "f69d7f59-ea62-11ed-b10a-08c0eb31fffb",
            "price": 0,
            "totalPrice": 0,
            "discountPrice": 0,
            "totalDiscountPrice": 0,
            "quantity": 1,
            "originalQuantity": 1,
            "identificationMark": null,
            "updatedDateTime": "2023-06-08T07:54:41Z"
        }
    ],
    "createdDateTime": "2023-06-08T07:54:41Z",
    "deliveredDateTime": null,
    "expectedDateTime": null,
    "updatedDateTime": "2023-06-08T07:54:41Z",
    "totalDiscountPrice": 0,
    "totalPrice": 0,
    "deliveryPrice": 0
}
``` 
**Запрос**

| Имя               | Значение           | Обязательность | Где    | Описание                                   | Пример                                                                                      |
|-------------------|--------------------|----------------|--------|--------------------------------------------|---------------------------------------------------------------------------------------------|
| authorization     | Token type + token | Да             | header | Токен доступа к ресурсу                    | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b |
| storeId           | UUID               | Да             | path   | Идентификатор склада                       | d6e24282-c4e3-11ea-b7de-0050560306e1                                                        |
| orderId           | String             | Да             | path   | Идентификатор заказа на стороне Самоката   | 0d15a7e5-988e-4672-a9ec-ce3dffaa88e4                                                        |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                        | Message                                 | Message                                                                    |
|-------------|-----------------------------|-----------------------------------------|----------------------------------------------------------------------------|
| 200         |                             |                                         | Успешный ответ                                                             |
| 400         | INVALID_PAYMENT_TYPE        | Invalid payment type                    | Передан недопустимый тип оплаты (Допустимый — prepaid)                     |
| 400         | ORDER_PRICE_MISMATCH        | Order price mismatch                    | Несовпадение цены заказа с первоначальной ценой (неподтвержденного заказа) |
| 400         | ORDER_STATUS_MISMATCH       | Order status mismatch                   | Несовпадение статуса заказа                                                |
| 401         | UNAUTHORIZED                |                                         | Неавторизованный запрос                                                    |
| 404         | ORDER_NOT_FOUND             | Order not found                         | Заказ не найден                                                            |
| 500         | INTERNAL_SERVER_ERROR       | something wrong. please try again later | Внутренняя ошибка сервера                                                  |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```
### Отмена заказа
Метод отмены созданного заказа.
Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/{storeId}/orders/{orderId}/cancellation
```  
Пример запроса:  
``` py 
curl --location --request PUT 'https://partners-api-sandbox.samokat.io/showcase/v1/stores/5e5aaf5a-90aa-11ea-b6ec-0050560306e1/orders/0d15a7e5-988e-4672-a9ec-ce3dffaa88e4/cancellation' \  
--header 'Accept: */*' \  
--header 'Authorization: Bearer 86ea424634665840039fec370a9bd58fa6afc52a83baf51b4dec86dc97fdf838'  
```

Пример ответа:  
``` py 
{
    "id": "0d15a7e5-988e-4672-a9ec-ce3dffaa88e4",
    "originalOrderId": "kshudabaevtest_204",
    "status": "cancelled",
    "orderLinesChanged": false,
    "orderLines": [
        {
            "productId": "f69d7f59-ea62-11ed-b10a-08c0eb31fffb",
            "price": 0,
            "totalPrice": 0,
            "discountPrice": 0,
            "totalDiscountPrice": 0,
            "quantity": 1,
            "originalQuantity": 1,
            "identificationMark": null,
            "updatedDateTime": "2023-06-08T07:54:41Z"
        }
    ],
    "createdDateTime": "2023-06-08T07:54:41Z",
    "deliveredDateTime": null,
    "expectedDateTime": null,
    "updatedDateTime": "2023-06-08T08:01:14Z",
    "totalDiscountPrice": 0,
    "totalPrice": 0,
    "deliveryPrice": 0
}
``` 
**Запрос**

| Имя               | Значение           | Обязательность | Где    | Описание                                     | Пример                                                                                      |
|-------------------|--------------------|----------------|--------|----------------------------------------------|---------------------------------------------------------------------------------------------|
| authorization     | Token type + token | Да             | header | Токен доступа к ресурсу                      | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b |
| storeId           | UUID               | Да             | path   | Идентификат ор склада                        | d6e24282-c4e3-11ea-b7de-0050560306e1                                                        |
| orderId           | String             | Да             | path   | Идентифика тор заказа на стороне Самоката    | 0d15a7e5-988e-4672-a9ec-ce3dffaa88e4                                                        |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                           | Message                                  | Message                                |
|-------------|--------------------------------|------------------------------------------|----------------------------------------|
| 200         |                                |                                          | Успешный ответ                         |
| 400         | CAN_NOT_CHAN GE_ORDER_STATUS   | cannot change order status               | Недопустимый статус заказа для отмены  |
| 401         | UNAUTHORIZED                   |                                          | Неавторизованный запрос                |
| 404         | ORDER_NOT_FOUND                | Order not found                          | Заказ не найден                        |
| 500         | INTERNAL_SERVER_ERROR          | something wrong. please try again later  | Внутренняя ошибка сервера              |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```

### Получение информации о заказе
Метод предоставления информации о текущем состоянии заказа.
Путь:  
``` py 
https://partners-api-sandbox.samokat.io/showcase/v1/stores/{storeId}/orders/{orderId}
```  
Пример запроса:  
``` py 
curl --location 'https://partners-api-sandbox.samokat.io/showcase/v1/stores/5e5aaf5a-90aa-11ea-b6ec-0050560306e1/orders/0d15a7e5-988e-4672-a9ec-ce3dffaa88e4' \
--header 'Accept: */*' \
--header 'Authorization: Bearer 0c2714754b2b34d6b3d02c26dbb468de4739296b91e90b8ea54504d92e0fa5dd' 
```

Пример ответа:  
``` py 
{
    "id": "0d15a7e5-988e-4672-a9ec-ce3dffaa88e4",
    "originalOrderId": "kshudabaevtest_204",
    "status": "cancelled",
    "orderLinesChanged": false,
    "orderLines": [
        {
            "productId": "f69d7f59-ea62-11ed-b10a-08c0eb31fffb",
            "price": 0,
            "totalPrice": 0,
            "discountPrice": 0,
            "totalDiscountPrice": 0,
            "quantity": 1,
            "originalQuantity": 1,
            "identificationMark": null,
            "updatedDateTime": "2023-06-08T07:54:41Z"
        }
    ],
    "createdDateTime": "2023-06-08T07:54:41Z",
    "deliveredDateTime": null,
    "expectedDateTime": null,
    "updatedDateTime": "2023-06-08T08:01:14Z",
    "totalDiscountPrice": 0,
    "totalPrice": 0,
    "deliveryPrice": 0
}
``` 
**Запрос**

| Имя               | Значение           | Обязательность | Где    | Описание                                   | Пример                                                                                      |
|-------------------|--------------------|----------------|--------|--------------------------------------------|---------------------------------------------------------------------------------------------|
| authorization     | Token type + token | Да             | header | Токен доступа к ресурсу                    | Authorization: Bearer 181210f8f9c7 79c26da1d9b 2075bde0127 302ee0e3fca 38c9a83f5b1 dd8e5d3b |
| storeId           | UUID               | Да             | path   | Идентификатор склада                       | d6e24282-c4e3-11ea-b7de-0050560306e1                                                        |
| orderId           | String             | Да             | path   | Идентификатор заказа на стороне Самоката   | 0d15a7e5-988e-4672-a9ec-ce3dffaa88e4                                                        |

**Коды ответа и возможные ошибки**

| HTTP Status | Code                           | Message                                  | Message                                |
|-------------|--------------------------------|------------------------------------------|----------------------------------------|
| 200         |                                |                                          | Успешный ответ                         |
| 401         | UNAUTHORIZED                   |                                          | Неавторизованный запрос                |
| 404         | ORDER_NOT_FOUND                | Order not found                          | Заказ не найден                        |
| 500         | INTERNAL_SERVER_ERROR          | something wrong. please try again later  | Внутренняя ошибка сервера              |

Пример структуры ответа с ошибкой:  
``` py 
{
    "code": "INTERNAL_SERVER_ERROR",
    "message": "something wrong. please try again later"
}
```